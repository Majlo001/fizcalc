package com.example.fizcalc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Velocity extends AppCompatActivity {

    EditText pred, droga, czas;
    TextView wynik;
    Button enter;

    String p,d,c;
    double V,s,t,calc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_velocity);

        pred = findViewById(R.id.predkosc);
        droga = findViewById(R.id.droga);
        czas = findViewById(R.id.czas);
        wynik = findViewById(R.id.wynik);
        enter = findViewById(R.id.enter);


        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p = pred.getText().toString();
                d = droga.getText().toString();
                c = czas.getText().toString();


                if (p.equals("") || p.equals("?")){
                    s = Integer.parseInt(d);
                    t = Integer.parseInt(c);
                    calc = s/t;
                    wynik.setText(String.valueOf("Prędkość = "+calc));
                }
                else if(d.equals("") || d.equals("?")){
                    V = Integer.parseInt(p);
                    t = Integer.parseInt(c);
                    calc = V*t;
                    wynik.setText(String.valueOf("Droga = "+calc));
                }
                else if(c.equals("") || c.equals("?")){
                    V = Integer.parseInt(p);
                    s = Integer.parseInt(d);
                    calc = V/s;
                    wynik.setText(String.valueOf("Czas = "+calc));
                }
                else{
                    wynik.setText("Chyba coś źle wpisałeś");
                }
            }
        });

    }
}