package com.example.fizcalc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;


public class Calculator extends AppCompatActivity {

    Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9;
    Button bClear,bDot,bPlus,bMinus,bEqual,bDivision,bBracket,bMultiply, bPercent;

    TextView input, output;

    String calculations, outputString;
    boolean bracketsCheck = false;

    public void setClear(){
        input.setText("");
    }
//    public void emptyInput(){
//        calculations = input.getText().toString();
//        outputString = output.getText().toString();
//        if (calculations.equals("")){
//            if(!outputString.equals("")){
//                calculations = outputString;
//            }
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        b0 = findViewById(R.id.b0);
        b1 = findViewById(R.id.b1);
        b2 = findViewById(R.id.b2);
        b3 = findViewById(R.id.b3);
        b4 = findViewById(R.id.b4);
        b5 = findViewById(R.id.b5);
        b6 = findViewById(R.id.b6);
        b7 = findViewById(R.id.b7);
        b8 = findViewById(R.id.b8);
        b9 = findViewById(R.id.b9);

        bClear = findViewById(R.id.clear);

        bPlus = findViewById(R.id.plus);
        bMinus = findViewById(R.id.minus);
        bMultiply = findViewById(R.id.multiplication);
        bDivision = findViewById(R.id.division);
        bEqual = findViewById(R.id.equal);
        bPercent = findViewById(R.id.percent);

        bDot = findViewById(R.id.dot);
        bBracket = findViewById(R.id.brackets);

        input = findViewById(R.id.input);
        output = findViewById(R.id.output);



        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setClear();
                output.setText("");
            }
        });


        // Funkcje dodające liczby
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"0");
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"1");
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"2");
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"5");
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"9");
            }
        });


        // Funkcje dodające znaki
        bMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"-");
            }
        });
        bPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"+");
            }
        });
        bDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"/");
            }
        });
        bMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"×");
            }
        });
        bDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+".");
            }
        });
        bPercent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();
                input.setText(calculations+"%");
            }
        });
        bEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();

                calculations = calculations.replaceAll("×","*");
                calculations = calculations.replaceAll("%","/100");

                Expression expression = new ExpressionBuilder(calculations).build();
                try {
                    double result = expression.evaluate();

                    output.setText(Double.toString(result));

                }catch (Exception e){
                    System.out.println("Message: "+e.getMessage());
                }
                setClear();
            }
        });
        bBracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculations = input.getText().toString();

                if(bracketsCheck == true){
                    calculations = input.getText().toString();
                    input.setText(calculations+")");
                    bracketsCheck = false;
                }else{
                    calculations = input.getText().toString();
                    input.setText(calculations+"(");
                    bracketsCheck = true;
                }
            }
        });
    }
}